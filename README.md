MILESTONES:

0. ~~Set up environment, makefile~~
1. ~~Write program which prints to output, compile, load on BOCHS~~
2. ~~Create bootloader which loads above program~~
3. ~~Create output function, call that function~~
4. ~~Create FAT disk builder, in makefile~~
5. ~~Create file allocation table lookup function~~
6. ~~Create program loader function~~
7. ~~Set resident program to use FAT lookup to load print program~~
8. ~~Write keyboard input function~~
9. ~~Write echo program.~~
10. Write user-level header file.
11. Make user programs to test OS
    1. Age-guessing program
    2. Graphing program

OVERVIEW:

    Memory is going to be a total of 64k. This makes it easy, because we never need to use segments.
    The kernel will be loaded starting from the 32k mark (0x8000).
    The kernel's entry point will be 0x8000.
    The kernel will then load from 0x500 to 0x5ff with jumps into functions.
    New programs will be loaded starting from 0x600.
    The stack grows down from 0x7FFF. Can safely grow all the way down to 0x7E00.

Memory map:

    0x500-0x5FF: Transient program area -- a set of long jumps, every 8 bytes
    0x600-??: Program working area -- programs get loaded into this space, and then are executed by jumping to 0x600
    0x7A00-0x7BFF: File lookup table
    0x7C00-0x7DFF: Bootloader
    0x7E00-0x7FFF: Stack
    0x8000-0x8100: Copy of TPA
    0x8100-??: The rest of the kernel
