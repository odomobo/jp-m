#!/usr/bin/perl
# Combines multiple files into a simple FAT structure.

#use File::stat;

$total_sectors = shift;

$outfile = shift;
open(my $fh, ">", $outfile);
truncate $fh, 0;

$bootldr = shift;
open(my $bfh, "<", $bootldr);

# write bootloader
while(read($bfh, $data, 1000) != 0) {
    print $fh $data; # echo line read
}

# sign bootloader
seek($fh, 510, 0);
print $fh "\x55\xAA";

printf ">>>> Wrote %s as as bootloader to sector 0\n", $bootldr;

$current_sector = 2;
$current_record = 0;

while (my $filename = shift) {
    use integer;
    open(my $tfh, "<", $filename);
    my $size = (stat($tfh))[7];
    
    # print record
    seek($fh, 512 + (16*$current_record), 0);
    printf $fh $filename;
    seek($fh, 512 + (16*$current_record) + 12, 0);
    printf $fh pack("SS", $current_sector, $size);
    
    # print sector
    seek($fh, 512*$current_sector, 0);
    while(read($tfh, $data, 1000) != 0) {
        print $fh $data; # echo line read
    }
    
    printf ">>>> Wrote %s as record %s, to sector %s with length %s\n", $filename, $current_record, $current_sector, $size;
    close($tfh);
    
    $current_sector = $current_sector + ($size/512);
    if  ($size%512 > 0) {
        $current_sector += 1;
    }
    
    $current_record += 1;
}

seek($fh, 512*$total_sectors - 1, 0);
printf $fh "\x00";

printf ">>>> Ensuring image is %s sectors long\n", $total_sectors;

close($fh);