#!/bin/bash
# intelligently combines .text with .rodata

shopt -s expand_aliases
alias log=">&2 printf"
log ">>>>    "

if [ $# != 3 ]
then
    log "usage: build_com.sh file.text file.rodata file.sym\n"
    exit 1
fi

textfile="$1"
rodatafile="$2"
symfile="$3"

textfile_size=$(stat -c %s $textfile)
rodatafile_size=$(stat -c %s $rodatafile)

# first output .text
log "Outputting $textfile"
cat $textfile

# if .rodata is empty, then we are finished
if (( $rodatafile_size == 0 ))
then
    log "\n" # force a newline on informational output
    exit 0
fi
log "; "

textstart=$(grep ' \.text$' $symfile | sed 's/ .*//')
rodatastart=$(grep ' \.rodata$' $symfile | sed 's/ .*//')

if [ -z $textstart ] || [ -z $rodatastart ]
then
    log "Symbol file missing data\n"
    exit 1
fi

padding=$(( 0x$rodatastart - 0x$textstart - $textfile_size ))
if (( $padding > 0 ))
then
    log "Padding by $padding bytes; "
    while (( $padding > 0 ))
    do
        printf "\0"
        (( padding = $padding - 1 ))
    done
elif (( $padding == 0 ))
then
    log "No padding required; "
else
    log "Calculation error: negative padding\n"
    exit 1
fi


log "Appending $rodatafile\n"
cat $rodatafile
