asm(".code16gcc"); /* compile 16-bit instructions */
#include "filesys.h"

void get_file(short inode, void * ptr) {
    char * cptr = ptr;
    short remaining;
    struct dirent * de;
    short sector;
    
    de = (struct dirent *)FILE_TABLE;
    de += inode;
    remaining = de->length;
    sector = de->start_sector;
    
    while (remaining > 0) {
        read_sect(sector, cptr);
        
        remaining -= 512;
        cptr += 512;
        sector++;
    }
}


