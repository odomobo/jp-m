#ifndef FILESYS_H
#define FILESYS_H

#define FILE_TABLE 0x7A00

/* c-only section */
#ifndef __ASSEMBLER__

#define FAT ((struct dirent * )FILE_TABLE);

void get_file(short file_no, void * ptr);
void read_sect(short sector, void * mem_loc);

struct dirent {
    char                name[12];
    unsigned short      start_sector;
    unsigned short      length;
};

#endif /*__ASSEMBLER__*/
#endif /*FILESYS_H*/