#include "bootldr.h"

# Assemble for 16-bit mode
.code16gcc
.globl start
start:
    # clear interrupts set by BIOS
    cli
    
    # Zero data segment registers DS, ES, and SS.
    movw $0, %ax
    movw %ax, %ds # −> Data Segment
    movw %ax, %es # −> Extra Segment
    movw %ax, %ss # −> Stack Segment
    
    # set up stack
    movw $KERNEL_START, %sp
    movw $KERNEL_START, %bp
    
    # load kernel into memory
    call load_kernel
    
    # run
    ljmp $0, $KERNEL_START
    
    # shouldn't reach here
spin:
    jmp spin
