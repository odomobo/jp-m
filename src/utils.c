asm(".code16gcc"); /* compile 16-bit instructions */
#include "utils.h"
#include "bootldr.h"
#include "filesys.h"

void put_string(char * str) {
    while (*str) {
        put_char(*str);
        str++;
    }
}

void put_uint(unsigned int s, short base) {
    char * chars = "0123456789abcdef";
    char buf[20];
    char * t = &buf[19];
    buf[19] = 0;
    
    do {
        t--;
        *t = chars[s%base];
        s /= base;
    } while (s > 0);
    put_string(t);
}

void put_int(int s, short base) {
    if (s < 0) {
        put_char('-');
        s = -s;
    }
    
    put_uint(s, base);
}

void printf(char * str, ...) {
    int escape = 0;
    void * args = &str;
    args += sizeof(char *);
    
    while (*str) {
        if (escape) {
            switch (*str) {
                case 'd':
                    put_int(*((int *)args), 10);
                    args += sizeof(int);
                    break;
                case 'x':
                    put_string("0x");
                    put_uint(*((unsigned int *)args), 16);
                    args += sizeof(unsigned int);
                    break;
                case 's':
                    put_string(*((char **)args));
                    args += sizeof(char *);
                    break;
                case '%':
                    put_char('%');
                    break;
                default:
                    put_string("ERROR");
                    put_char(*str);
            }
            escape = 0;
        } else if (*str == '%') {
            escape = 1;
        } else {
            put_char(*str);
            escape = 0;
        }
        str++;
    }
}

void memcpy(char * src, char * dst, short size) {
    for(; size > 0; size--) {
        *dst = *src;
        dst++;
        src++;
    }
}

/* ret 0 on no match, 1 on match */
short strmatch(char * s1, char * s2) {
    while (*s1 != 0 && *s2 != 0) {
        if (*s1 != *s2) 
            return 0;
        
        s1++; s2++;
    }
    if (*s1 != *s2) {
        return 0;
    } else {
        return 1;
    }
}

void get_string(char * str, short length) {
    short pos = 0;
    while (1) {
        char c = wait_for_keystroke();
        if (c == '\b') {
            if (pos > 0) {
                short row, col;
                get_cursor_position(&row, &col);
                /* if the column is 0, go to the end of the previous line */
                if (col == 0 && row > 0) {
                    /* we're going to be very naughty, and assume that the terminal is 80 characters wide. We should really check dynamically */
                    locate(row-1, 79);
                    put_char(' ');
                    locate(row-1, 79);
                } else {
                    put_string("\b \b");
                }
                pos--;
            }
        } else if (c == '\r') {
            put_string("\r\n");
            break;
        } else if (pos+1 >= length) {
            continue;
        } else if (c >= ' ' && c < 127) {
            put_char(c);
            str[pos] = c;
            pos++;
        }
    }
    str[pos] = '\0';
}

int get_inode(char * filename) {
    short inode = 0;
    struct dirent * de = FAT;
    while (de->name[0] != 0) {
        if (strmatch(filename, &de->name[0])) {
            return inode;
        }
        de++; inode++;
    }
    return -1;
}