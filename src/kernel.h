#ifndef KERNEL_H
#define KERNEL_H

#define TPA_START 0x500
#define TPA_SIZE 0x100
#define PROGRAM_START 0x600

/* c-only section */
#ifndef __ASSEMBLER__

int run_program(void);

#endif /*__ASSEMBLER__*/
#endif /*KERNEL_H*/