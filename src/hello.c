asm(".code16gcc"); /* compile 16-bit instructions */
#include "stdinc.h"

/* entry point is implicitly first function of first file */
int start(void) {
    printf("Hello!\r\n");
    return 0;
}
