asm(".code16gcc"); /* compile 16-bit instructions */
#include "bootldr.h"
#include "filesys.h"

void load_kernel(void) {
    read_sect(1, (char *)FILE_TABLE);
    get_file(0, (char *)KERNEL_START);
}
