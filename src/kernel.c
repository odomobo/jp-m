asm(".code16gcc"); /* compile 16-bit instructions */
#include "kernel.h"
#include "utils.h"
#include "filesys.h"
#include "bootldr.h"

void cmd(void) {
    char buf[200];
    /* copies first 0x100 bytes */
    memcpy((char * )KERNEL_START, (char * )TPA_START, TPA_SIZE);
    
    clear_screen();
    printf("Welcome to jp/m!\r\n\r\n");
    
    do {
        put_string("> ");
        get_string(&buf[0], sizeof(buf));
        if (strmatch(&buf[0], "ls")) {
            struct dirent * de = FAT;
            
            while (de->name[0] != 0) {
                printf("%s %d\r\n", &de->name[0], de->length);
                de++;
            }
        } else {
            int inode = get_inode(&buf[0]);
            if (inode >= 0) {
                get_file(inode, (void *)PROGRAM_START);
                run_program();
            } else {
                printf("No such file or command: \"%s\"\r\n", &buf[0]);
            }
        }
    } while(1);
}