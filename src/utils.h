#ifndef UTILS_H
#define UTILS_H

#define NULL ((void *)0)

/* assembly functions */
void clear_screen(void);
char wait_for_keystroke(void);
short check_for_keystroke(void);
void put_char(char c);
void locate(short row, short col);
void get_cursor_position(short * row, short * col);

/* c functions */
void put_string(char * str);
void put_uint(unsigned int s, short base);
void put_int(int s, short base);
void printf(char * str, ...);
void memcpy(char * src, char * dst, short size);
short strmatch(char * s1, char * s2);
void get_string(char * str, short length);
int get_inode(char * filename);

#endif /*UTILS_H*/
